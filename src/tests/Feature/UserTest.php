<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class UserTest extends TestCase
{
    //Todo It's an example of a test, I could write many tests for all endpoints if I had time.
    public function test_otp(): void
    {
        $phone = '09363552928';
        $code = rand(1000, 9999);
        Cache::set($phone, $code, config('app.otp_cache_time'));

        $response = $this->post('api/v1/otp/verify', [
            'mobile' => $phone,
            'code' => $code
        ], [
            'accept' => 'application/json'
        ]);

        $response->assertStatus(200);

    }
}
