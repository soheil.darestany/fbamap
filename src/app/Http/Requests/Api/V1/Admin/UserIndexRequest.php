<?php

namespace App\Http\Requests\Api\V1\Admin;

use App\Models\enums\UserRole;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserIndexRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'nullable',
            'email' => 'nullable',
            'mobile' => 'nullable',
            'role' => [Rule::enum(UserRole::class)]
        ];
    }
}
