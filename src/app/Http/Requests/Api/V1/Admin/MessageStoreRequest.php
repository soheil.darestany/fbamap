<?php

namespace App\Http\Requests\Api\V1\Admin;

use App\Models\Ticket;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MessageStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'ticket_id' => ["required", 'numeric', Rule::exists(Ticket::class, 'id')],
            'context'  => 'required|regex:/^[a-zA-Z0-9\s]+$/'
        ];
    }
}
