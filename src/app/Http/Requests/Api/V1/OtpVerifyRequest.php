<?php

namespace App\Http\Requests\Api\V1;

use App\Rules\Api\V1\OtpCacheRule;
use App\Rules\Api\V1\OtpCodeCheckRule;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class OtpVerifyRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            "mobile" => ['required_without:email', 'regex:/[0]{1}[0-9]{10}/'],
            "email" => ['required_without:mobile', 'email'],
            'code' => ['required', 'numeric', 'min:1000','max:9999', new OtpCodeCheckRule]
        ];
    }
}
