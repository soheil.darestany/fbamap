<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\User\MessageStoreRequest;
use App\Http\Requests\Api\V1\User\TicketStoreRequest;
use App\Http\Requests\Api\V1\User\TicketUpdateRequest;
use App\Http\Resources\Api\V1\User\MessagesResource;
use App\Http\Resources\Api\V1\User\TicketResource;
use App\Http\Resources\Api\V1\User\TicketsResource;
use App\Models\Message;
use App\Models\Ticket;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class TicketController extends Controller
{
    public function index(): AnonymousResourceCollection
    {
        return TicketsResource::collection(Ticket::mySelf()->orderBy('created_at','desc')->paginate());
    }
    public function show($id): TicketResource
    {
        return new TicketResource(Ticket::mySelf()->find($id));
    }

    public function store(TicketStoreRequest $request): TicketResource
    {
        $input=$request->validated();
        $ticket=Ticket::create($input);
        $ticket->messages()->create($input['message']);
        return new TicketResource($ticket);
    }

    public function update(TicketUpdateRequest $request, $id): TicketResource
    {
        $ticket=Ticket::mySelf()->find($id);
        $ticket->update($request->validated());
        return new TicketResource($ticket);

    }
    public function messages($ticketId): AnonymousResourceCollection
    {
        return MessagesResource::collection(Ticket::mySelf()->where('id',$ticketId)->messages()->paginate());
    }

    public function messageStore(MessageStoreRequest $request,$ticketId): MessagesResource
    {
        return new MessagesResource(Ticket::mySelf()->where('id',$ticketId)->messages()->create($request->validated()));
    }
}
