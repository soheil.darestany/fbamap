<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Admin\TicketIndexRequest;
use App\Http\Requests\Api\V1\Admin\TicketUpdateRequest;
use App\Http\Resources\Api\V1\Admin\TicketResource;
use App\Http\Resources\Api\V1\Admin\TicketsResource;
use App\Repositories\V1\Admin\Contracts\TicketRepositoryInterface;


class TicketController extends Controller
{
    public function __construct(private readonly TicketRepositoryInterface $repository)
    {
    }

    public function index(TicketIndexRequest $request): TicketsResource
    {
        return TicketsResource::make($this->repository->all($request->validated()));
    }
    public function show($id): TicketResource
    {
        return new TicketResource($this->repository->find($id));
    }

    public function edit(TicketUpdateRequest $request,$id): TicketResource
    {
        return new TicketResource($this->repository->update($request->validated(),$id));
    }
}
