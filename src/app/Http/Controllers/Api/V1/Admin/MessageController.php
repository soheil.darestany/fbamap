<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Admin\MessageStoreRequest;
use App\Http\Requests\Api\V1\Admin\MessageUpdateRequest;
use App\Http\Resources\Api\V1\Admin\MessageResource;
use App\Http\Resources\Api\V1\Admin\MessagesResource;
use App\Repositories\V1\Admin\Contracts\MessageRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class MessageController extends Controller
{
    public function __construct(private readonly MessageRepositoryInterface $repository)
    {
    }

    public function index($ticketId): AnonymousResourceCollection
    {
        return MessagesResource::collection($this->repository->all(filter: ['ticket_id' => $ticketId]));
    }

    public function show($id): MessageResource
    {
        return new MessageResource($this->repository->find($id));
    }

    public function store(MessageStoreRequest $request): MessageResource
    {
        return new MessageResource($this->repository->create($request->validated()));
    }

    public function edit(MessageUpdateRequest $request, $id): MessageResource
    {
        return new MessageResource($this->repository->update($request->validated(), $id));
    }
}
