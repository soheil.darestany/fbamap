<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Admin\UserIndexRequest;
use App\Http\Requests\Api\V1\Admin\UserUpdateRequest;
use App\Http\Resources\Api\V1\Admin\UserResource;
use App\Http\Resources\Api\V1\Admin\UsersResource;
use App\Repositories\V1\Admin\Contracts\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class UserController extends Controller
{
    public function __construct(private readonly UserRepositoryInterface $repository)
    {
    }

    public function index(UserIndexRequest $request): AnonymousResourceCollection
    {
        return UsersResource::collection($this->repository->all($request->validated()));
    }

    public function edit(UserUpdateRequest $request,$id): UserResource
    {
        return new UserResource($this->repository->update($request->validated(),$id));
    }

    public function show($id): UserResource
    {
        return new UserResource($this->repository->find($id));
    }
}
