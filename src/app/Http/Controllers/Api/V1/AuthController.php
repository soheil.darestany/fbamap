<?php

namespace App\Http\Controllers\Api\V1;

use App\Events\Otp;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\OtpRequest;
use App\Http\Requests\Api\V1\OtpVerifyRequest;
use App\Http\Resources\Api\V1\OtpResource;
use App\Http\Resources\Api\V1\OtpVerifyResource;
use App\Models\User;
use Illuminate\Support\Facades\Cache;

class AuthController extends Controller
{
    public function otp(OtpRequest $request): OtpResource
    {
        $input = $request->validated();
        $code = rand(1000, 9999);
        Cache::set($input['mobile'] ?? $input['email'], $code, config('app.otp_cache_time'));
        event(new Otp($code, $input));
        /*
         * I put otpCode in result for test because I didn't have any provider for
         *  send otpCode
         * */
        return new OtpResource(collect([
            'message' => __('messages.otp'),
            'code' => $code
        ]));

    }

    public function verifyOtp(OtpVerifyRequest $request): OtpVerifyResource
    {
        $input = $request->validated();
        if (isset($input['mobile'])) {
            $user = User::firstOrCreate(['mobile' => $input['mobile']]);
        } else {
            $user = User::firstOrCreate(['email' => $input['email']]);
        }
        return new OtpVerifyResource(collect(["token" => $user->createToken($user->role->value ?? 'user')->plainTextToken]));

    }
}
