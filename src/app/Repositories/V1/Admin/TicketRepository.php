<?php

namespace App\Repositories\V1\Admin;

use App\Models\Ticket;
use App\Repositories\V1\Admin\Contracts\TicketRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class TicketRepository extends BaseRepository implements TicketRepositoryInterface
{
    public function __construct(Ticket $ticket)
    {
        $this->model = $ticket;
    }

    public function status_change(array $data, $id): Model
    {
        $query = $this->find($id);
        $query->update([$query->status = $data['status']]);

        return $query;
    }
}
