<?php
declare(strict_types=1);

namespace App\Repositories\V1\Admin;

use App\Repositories\V1\Admin\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class BaseRepository implements RepositoryInterface
{
    protected Model $model;

    /**
     * @param array $filter
     * @param array|null $with
     * @param array|null $select
     * @return LengthAwarePaginator
     */
    public function all(array $filter, ?array $with = [], ?array $select = []): LengthAwarePaginator
    {
        return $this->model->withoutGlobalScopes()->with($with)->where(function ($q) use ($filter) {
            foreach ($filter as $key => $value) {
                $q->where($key, "like", "{$value}%");
            }
        })->whereRaw("deleted_at is null")->Paginate(config('app.pageCount'));
    }

    /**
     * @param array $data
     * @return Model
     */
    public function create(array $data): Model
    {
        return $this->model->create($data);
    }

    /**
     * @param $id
     * @param array|null $with
     * @return Model
     */
    public function find($id, ?array $with = []): Model
    {
        return $this->model->withoutGlobalScopes()->with($with)->whereRaw("deleted_at is null")->findOrFail($id);
    }

    /**
     * @param array $data
     * @param $id
     * @return Model
     */
    public function update(array $data, $id): Model
    {
        $query = $this->find($id);
        $query->update($data);
        return $query;
    }

    /**
     * @param string $id
     * @return bool
     */
    public function delete(string $id): bool
    {
        return $this->find($id)->delete();
    }



}
