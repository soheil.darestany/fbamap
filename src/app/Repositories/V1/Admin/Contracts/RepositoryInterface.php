<?php
declare(strict_types=1);

namespace App\Repositories\V1\Admin\Contracts;

interface RepositoryInterface
{
    public function all(array $filter, ?array $with = [], ?array $select = []);

    public function create(array $data);

    public function find($id, ?array $with = []);

    public function update(array $data, $id);

    public function delete(string $id);

}
