<?php

namespace App\Repositories\V1\Admin\Contracts;

use Illuminate\Database\Eloquent\Model;

interface TicketRepositoryInterface extends RepositoryInterface
{
    public function status_change(array $data, $id): Model;
}
