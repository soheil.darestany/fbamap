<?php

namespace App\Repositories\V1\Admin\Contracts;


use Illuminate\Database\Eloquent\Model;

interface UserRepositoryInterface extends RepositoryInterface
{
    public function role_change(array $data, $id): Model;
}
