<?php

namespace App\Repositories\V1\Admin;

use App\Models\User;
use App\Repositories\V1\Admin\Contracts\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Model;


class UserRepository extends BaseRepository implements UserRepositoryInterface
{

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    /**
     * @param array $data
     * @param $id
     * @return Model
     */
    public function role_change(array $data, $id): Model
    {
        $query = $this->find($id);
        $query->update([$query->role = $data['role']]);

        return $query;
    }
}
