<?php

namespace App\Listeners;

use App\Events\Otp;
use App\Services\OtpNotification\Email\EmailHandler;
use App\Services\OtpNotification\Email\Strategy\Mailgun;
use App\Services\OtpNotification\OtpFactory;
use App\Services\OtpNotification\Sms\Kavenegar;
use App\Services\OtpNotification\Sms\SmsHandler;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendOtpNotification
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(Otp $event): void
    {
        if(isset($event->params['mobile'])){
            (new SmsHandler(new Kavenegar,$event->code,$event->params['mobile']))->run();
        }else{
            (new EmailHandler(new Mailgun,$event->code,$event->params['email']))->run();
        }
    }
}
