<?php

namespace App\Models;

use App\Models\enums\TicketStatus;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method static mySelf()
 */
class Ticket extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'status',
        'user_id'
    ];
    /**
     * @var string[]
     */
    protected $casts = [
        'status' => TicketStatus::class
    ];

    /**
     * @return BelongsTo
     */
    function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return void
     */
    public function setUserIdAttribute(): void
    {
        $this->attributes['user_id'] = request()->user()->id;
    }

    public function scopeMySelf($query)
    {
        return $query->whereDate('user_id',request()->user()->id);
    }
}
