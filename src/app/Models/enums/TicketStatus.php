<?php

namespace App\Models\enums;

enum TicketStatus: string
{
    case PENDING = 'pending';
    case SEEN_BY_ADMIN = 'seen by admin';
    case CANCEL = 'cancel';
    case COMPLETED = 'completed';
    case CLOSE = 'close';
}
