<?php

namespace App\Models\enums;

enum UserRole: string
{
    case ADMIN = 'admin';
    case USER = 'user';
}
