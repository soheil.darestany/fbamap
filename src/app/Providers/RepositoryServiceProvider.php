<?php
declare(strict_types=1);

namespace App\Providers;


use App\Repositories\V1\Admin\Contracts;
use App\Repositories\V1\Admin;
use Illuminate\Support\ServiceProvider;


class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Binding services.
     *
     * @return void
     */
    public array $bindings = [
        Contracts\UserRepositoryInterface::class => Admin\UserRepository::class,
        Contracts\TicketRepositoryInterface::class => Admin\TicketRepository::class,
        Contracts\MessageRepositoryInterface::class=> Admin\MessageRepository::class
    ];
}
