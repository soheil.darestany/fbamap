<?php

namespace App\Services\OtpNotification\Email;

class EmailHandler
{
    //Todo I can to use Facade pattern and delete construct method
    public function __construct(private readonly EmailInterface $emailClass,
                                private                         $code,
                                private                         $email)
    {
    }

    public function run(): void
    {
        $this->emailClass->send($this->code, $this->email);
    }
}
