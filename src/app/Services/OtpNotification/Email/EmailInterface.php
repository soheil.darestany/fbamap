<?php

namespace App\Services\OtpNotification\Email;
interface EmailInterface
{
    public function send(int $code, string $email): void;

}
