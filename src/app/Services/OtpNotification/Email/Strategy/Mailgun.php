<?php

namespace App\Services\OtpNotification\Email\Strategy;

use App\Services\OtpNotification\Email\EmailInterface;
use Illuminate\Support\Facades\Mail;

class Mailgun implements EmailInterface
{


    public function send(int $code, string $email): void
    {
        Mail::send('mail', ['code' => $code], function ($message) use ($email) {
            $message->to($email, 'Otp Code');
        });
    }

}
