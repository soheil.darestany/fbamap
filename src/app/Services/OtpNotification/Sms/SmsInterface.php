<?php

namespace App\Services\OtpNotification\Sms;

interface SmsInterface
{
    public function send(int $code,string $mobile);
}
