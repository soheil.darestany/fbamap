<?php

namespace App\Services\OtpNotification\Sms;

class SmsHandler
{
    //Todo I can to use Facade pattern and delete construct method
    public function __construct(private readonly SmsInterface $sms, private $code, private $mobile)
    {
    }

    public function run(): void
    {
        $this->sms->send($this->code,$this->mobile);
    }
}
