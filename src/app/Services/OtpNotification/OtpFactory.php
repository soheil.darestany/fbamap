<?php
namespace App\Services\OtpNotification;
use Exception;

class OtpFactory
{
    /**
     * @throws Exception
     */
    public static function build($notification, $code, $param)
    {

        if(class_exists($notification))
        {
            return new $notification($code, $param);
        }
        else {
            throw new Exception("Invalid product type given.");
        }
    }
}
