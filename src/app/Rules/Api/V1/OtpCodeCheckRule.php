<?php

namespace App\Rules\Api\V1;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Support\Facades\Cache;
use Illuminate\Translation\PotentiallyTranslatedString;

class OtpCodeCheckRule implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param string $attribute
     * @param mixed $value
     * @param Closure(string): PotentiallyTranslatedString $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $code = Cache::pull(request('mobile') ?? request('email'));
        if ($code!=$value){
            $fail(trans('The :attribute is not valid'));
        }
    }
}
