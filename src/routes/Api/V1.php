<?php

use App\Http\Controllers\Api\V1\Admin\MessageController;
use App\Http\Controllers\Api\V1\Admin\TicketController;
use App\Http\Controllers\Api\V1\Admin\UserController;
use App\Http\Controllers\Api\V1\AuthController;
use Illuminate\Support\Facades\Route;

Route::controller(AuthController::class)->prefix('otp')
    ->middleware('throttle:10,1')
    ->group(function () {
        Route::post('', 'otp');
        Route::post('verify', 'verifyOtp');
    });
Route::middleware('auth:sanctum')->group(function (){
   Route::middleware('role:admin')->group(function (){
        Route::controller(UserController::class)->prefix('user')->group(function (){
            Route::get('','index');
            Route::put('{id}','edit');
            Route::get('{id}','show');
        });
       Route::controller(TicketController::class)->prefix('ticket')->group(function (){
           Route::get('','index');
           Route::put('{id}','edit');
           Route::get('{id}','show');
       });
       Route::controller(MessageController::class)->prefix('message')->group(function (){
           Route::get('{ticketId}','index');
           Route::put('{id}','edit');
           Route::get('{id}','show');
           Route::post('','store');
       });
   });
   Route::middleware('role:user')->group(function (){
       Route::controller(\App\Http\Controllers\Api\V1\User\TicketController::class)->prefix('ticket')->group(function (){
           Route::get('','index');
           Route::put('{id}','edit');
           Route::get('{id}','show');
           Route::post('','store');
           Route::get('{ticketId}/messages','messages');
           Route::post('{ticketId}/messages','messageStore');
       });
   });
});
